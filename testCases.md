# Testing cases

Here four (4) test cases will be desribed. Both what to do, correct input into the console, expected result, and why we wish to perform these tests.

Program text when we interact with it is not described in the "correct input into the console" as this just take up space while providing little value.

## Test case 1

Start application, enter alarm close to current time, check that the alarm outputs the PING and alarm sound at correct time.
Input would be following:

> "./menu" \
> "s" \
> "2022-03-01 12:00:00" \

Expected result: \
Alarm 1 should be scheduled, and trigger the alarm activation.

The purpose of this test would be:

1. Ensure that alarms can be scheduled
2. Alarms alerts after X time.
3. The internal clock functions correctly.

## Test Case 2

Start application, enter several alarms, list all active alarms and check that number match entered alarms. Remove 1 alarm using 'c', check list of active to ensure the removed alarm is gone from the list.

> "./menu" \
> "s" \
> "2022-03-01 12:00:00"\
> "s" \
> "2022-03-01 12:00:00"\
> "s" \
> "2022-03-01 12:00:00" \
> "s" \
> "2022-03-01 12:00:00" \
> "s" \
> "2022-03-01 12:00:00" \
> "l"\
> "c" \
> "3" \
> "l" \
> "x"

Expected result: \
The first listing should contain alarm 1, 2, 3, 4, and 5. After c -> 3, the next listing should only contain alarm 1, 2, 4, and 5. Pressing "x" should then completely exit the program.

The purpose of this test would be:

1. Ensure list feature lists all entered alarms correctly. Not just a print of alarms but correctly print each alarms number. (Do not cancel the last one)
2. Ensure the cancel function correctly removes the specified alarm instead of just last or first.
3. Ensure the exit function properly quits the program and no alarms trigger after program exited.

## Test Case 3

Start application, enter several alarms, wait until all alarms go off. Use "top" to check processes (should see several zombies at this point). Enter any input ('s', 'l', or 'c') and check that the zombie processes disappear.

> "./menu" \
> "s" \
> "2022-03-01 12:00:00" \
> "s" \
> "2022-03-01 12:00:00" \
> "s" \
> "2022-03-01 12:00:00" \
> "s" \
> "2022-03-01 12:00:00" \
> "s" \
> "2022-03-01 12:00:00" \
> "top" \
> "l"

Expected result: \
Should appear 5 zombie processes using "top" after alarms go off. When entering "l" (or "c" or "s") all these zombie processes should disappear from the "top" display.

The purpose of this test would be:

1. Make sure several alarms can go off at the same time (independant processes).
2. Ensure that executed alarm zombies are correctly culled (should occur whenever user selects input in the main menu).

## Test Case 4

Start application, schedule 2 alarms. Cancel one of these alarms. Schedule a 3rd alarm, can be far in the future. Now wait until the 2 first alarms should have triggered and make sure that only 1 of them actually triggered.

> "./menu" \
> "s" \
> "2022-03-01 12:00:00"\
> "s" \
> "2022-03-01 12:00:00"\
> "s" \
> "l"\
> "c" \
> "2" \
> "s" \
> "2022-05-05 15:15:15"\
> "l"

Expected result: \
Alarm 1 triggeres while alarm 2 does not. The third scheduled alarm should become alarm 3, not alarm 2.

The purpose of this test would be:

1. Ensure that the selected alarm (#2) is cancelled and won't trigger.
2. Confirm that new alarms will continue filling up the 10 alarm array even though #2 is cancelled, if the last scheduled alarm is #2 this is wrong.
