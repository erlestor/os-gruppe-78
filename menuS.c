#include <stdio.h>
#include <stdlib.h> // Exit
#define __USE_XOPEN
#define _GNU_SOURCE
#include <time.h> 
#include <unistd.h> 
#include <signal.h> // For kill(2)
#include <sys/wait.h> // waitpid

// valid commands:
// s - schedule a new alarm
// l - list current alarms
// c - cancel a alarm
// x - exit the alarm system
time_t ltime;

int printInstruction();

int scheduleAlarm();

void printAlarms();

void cancelAlarm();

void executeAlarm();

void reapChildren();

struct alarm {
	time_t time;
	int active; // 1 for active,  0 for inactive
	pid_t pid; // Process ID of the child process.
	int cStatus;
};

struct alarm alarms[10];

int main(){
	main:
	reapChildren(); // Remove zombies from alarm processes
	printInstruction();
	char menuSelect;
	scanf(" %c", &menuSelect);
	switch (menuSelect) {
		case 's':
			// schedule alarm
			scheduleAlarm();
			// dont write goto main here. scheduleAlarm runs main() on end
		case 'l':
			// list all alarms
			printAlarms();
			goto main;
			
		case 'c':
			// cancel alarm
			cancelAlarm();
			goto main;

		case 'x':
			// exit
			printf("Goodbye!\n");
			// Kill any child processes
			for (int i = 0; i < 10; i++) {
				if (alarms[i].pid != 0) {
					kill(alarms[i].pid, SIGKILL);
				}
			}
			exit(1);
		default:
				printf("Invalid selection, please try again.\n");
				goto main;
	}
}

int printInstruction() {
    time(&ltime);
	printf("Welcome to the alarm clock! It is Currently %s\n", ctime(&ltime));
	printf("Please enter \"s\" (schedule), \"l\" (list), \"c\" (cancel), \"x\" (exit): \n");
}

int scheduleAlarm() {
	static int alarmIdx = 0;
	int *alarmPointer = &alarmIdx;
	if (alarmIdx > 9) {
		printf("Alarm system over capacity. Only 10 alarms allowed.\n");
		exit(0);
	}

	char temp;
	char dateString[20];
	printf("Schedule alarm at which date and time? ");
	scanf("%c", &temp);
	fgets(dateString, 20, stdin);
	printf("\n");

	struct alarm curAlarm;
	struct tm calendarTime;
	strptime(dateString, "%Y-%m-%d %H:%M:%S", &calendarTime); // nå er calendarTime en struct tm
	time_t timeT = mktime(&calendarTime);	// returnerer en time_t fra en struct tim

	// fork creates a new process. process stores a value we can use to indentify if its the child or parent process thats running the code
	curAlarm.pid = fork(); // Save the child PID in the parent process.
	if (curAlarm.pid == 0) {
		// child process runs here
		time_t currentTime;
		time(&currentTime);
		double diff_t = difftime(timeT, currentTime);
		printf("\nAlarm %i will execute in %f seconds\n", alarmIdx+1, diff_t);
		sleep(diff_t);
		executeAlarm();
		exit(0);
	}
    else {
		// parent process runs here
		curAlarm.active = 1;
		curAlarm.time = timeT;
		alarms[alarmIdx] = curAlarm;

		(*alarmPointer)++;
		main();
	}
}

void printAlarms() {
	for (int i = 0; i < 10; i++) {
		if (alarms[i].active) {
			printf("Alarm %i at time: %s", (i+1), ctime(&alarms[i].time));	// printer en time_t
		}
	}
	printf("\n");
}

void cancelAlarm() {
	// Alarm selection
	printf("Which alarm do you want to remove (index): ");
	int alarmIndex;
	scanf("%i", &alarmIndex);
	printf("Selected alarm to cancel: %i\n", alarmIndex);

	// Kill the child process of the alarm, to prevent alarm actually triggering.
	// Use SIGTERM if we want to let process clean up. SIGKILL is more brutal, but effective.
	kill(alarms[alarmIndex-1].pid, SIGTERM); 

	// Make new alarmstruct and place in the alarms array
	struct alarm emptyAlarm;
	emptyAlarm.active = 0;
	emptyAlarm.time = 0;
	alarms[alarmIndex-1] = emptyAlarm;
}

// Child process calls this, therefore incorrect memory location for alarms[] called. 
// Removed alarm reset. Dont have function to go back to alarm 0 anyway, so no issue just having value stored.
void executeAlarm() {
	printf("\a\nRING\n");
	if (fork() == 0) {
		execlp("mpg123", "mpg123", "./alarm.mp3", "-list-modules", NULL);
		exit(0);
	}

}

void reapChildren() {
	// Iterate through all the alarms
	// Listen for exit status from their children processes, save to cStatus variable.
	// WNOHANG -> Dont stop executing. Take value if exit status from child, continue if nothing found.
	for (int i = 0; i < 10; i++) {
		waitpid(alarms[i].pid, &(alarms[i].cStatus), WNOHANG);
	}
}